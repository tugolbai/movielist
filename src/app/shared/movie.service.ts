import { Subject } from 'rxjs';
import { Movie } from './movie.model';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
@Injectable()

export class MovieService {
  moviesChange = new Subject<Movie[]>();
  moviesFetching = new Subject<boolean>();
  private movies: Movie[] = [];

  constructor(private http: HttpClient) {
  }

  getMovies() {
    return this.movies.slice();
  }

  fetchMovies() {
    this.moviesFetching.next(true);
    this.http.get<{[id: string]: Movie}>('https://plovo-57f89-default-rtdb.firebaseio.com/movies.json')
      .pipe(map(result => {
        return Object.keys(result).map(id => {
          const movieData = result[id]
          return new Movie(id, movieData.name);
        });
      }))
      .subscribe(movies => {
        this.movies = movies;
        this.moviesChange.next(this.movies.slice());
        this.moviesFetching.next(false);
      }, () => {
        this.moviesFetching.next(false);
      });
  }

}
