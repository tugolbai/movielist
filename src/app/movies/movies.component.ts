import { Component, OnDestroy, OnInit } from '@angular/core';
import { Movie } from '../shared/movie.model';
import { Subscription } from 'rxjs';
import { MovieService } from '../shared/movie.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.css']
})

export class MoviesComponent implements OnInit, OnDestroy {
  movies!: Movie[];
  loading = false;
  loadingDelete = false;
  moviesChangeSubscription!: Subscription;
  moviesFetchingSubscription!: Subscription;

  constructor(private movieService: MovieService, private http: HttpClient) {}

  ngOnInit() {
    this.movies = this.movieService.getMovies();

    this.moviesChangeSubscription = this.movieService.moviesChange.subscribe((movies: Movie[]) => {
      this.movies = movies;
    });

    this.moviesFetchingSubscription = this.movieService.moviesFetching.subscribe((isFetching: boolean) => {
      this.loading = isFetching;
    });

    this.movieService.fetchMovies();
  }

  delete(movie: Movie) {
    this.loadingDelete = true;
    this.http.delete('https://plovo-57f89-default-rtdb.firebaseio.com/movies/' + movie.id + '.json').subscribe(() => {
      this.loadingDelete = false;
      this.movies = this.movieService.getMovies();

      this.moviesChangeSubscription = this.movieService.moviesChange.subscribe((movies: Movie[]) => {
        this.movies = movies;
      });
      this.movieService.fetchMovies();
    });
  }

  ngOnDestroy() {
    this.moviesChangeSubscription.unsubscribe();
    this.moviesFetchingSubscription.unsubscribe();
  }
}
