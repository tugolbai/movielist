import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MovieService } from '../shared/movie.service';

@Component({
  selector: 'app-new-movie',
  templateUrl: './new-movie.component.html',
  styleUrls: ['./new-movie.component.css']
})
export class NewMovieComponent implements OnInit {
  @ViewChild('nameInput') nameInput!: ElementRef;
  loading = false;

  constructor( private movieService: MovieService, private http: HttpClient) { }

  ngOnInit() {
  }

  addMovie() {
    const name: string = this.nameInput.nativeElement.value;
    if (!(name === '')) {
      const body = {name};

      this.loading = true;
      this.http.post('https://plovo-57f89-default-rtdb.firebaseio.com/movies.json', body)
        .subscribe(() => {
          this.loading = false;
          this.movieService.fetchMovies();
        });
    } else {
      alert('Введите название!')
    }
  }

}
